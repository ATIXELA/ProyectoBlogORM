<?php

use Faker\Generator as Faker;
use App\Models\CategoryLanguage;
use App\Models\Language;
use App\Models\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(CategoryLanguage::class, function (Faker $faker) {
  $factory->define(CategoryLanguage::class, function ($faker) use ($factory)  {
    return [
      //'categories_id' => $faker->rand('Category')->id,
      'categories_id' => $factory->create(Category::class)->id,
      //'languages_id' => $faker->rand('Language')->id,
      'languages_id' => $factory->create(Language::class)->id,
      'label'=>$faker->sentence(2),
      'slug'=>$faker->slug(3),
      'description'=>$faker->text(rand(10,15)),
    ];
});
