<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_posts', function (Blueprint $table) {
          $table->integer('posts_id')->unsigned();
          $table->integer('languages_id')->unsigned();

          $table->string('title')->nullable();
          $table->string('slug')->nullable();
          $table->text('content')->nullable();
          $table->unique(array('posts_id','languages_id'));

          //LLaves Foráneas
          $table->foreign('posts_id')->references('id')
                ->on('posts')->onDelete('cascade');
          $table->foreign('languages_id')->references('id')
                ->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_post');
    }
}
