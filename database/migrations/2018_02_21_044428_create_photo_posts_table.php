<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_posts', function (Blueprint $table) {
          $table->string('use')->nullable();
          $table->integer('orden')->nullable();
          $table->timestamp('created_at')->nullable();
          $table->timestamp('updated_at')->nullable();

          $table->integer('photos_id')->unsigned();
          $table->integer('posts_id')->unsigned();

          $table->unique(array('photos_id','posts_id'));
          //LLaves Foráneas
          $table->foreign('photos_id')->references('id')
                ->on('photos')->onDelete('cascade');
          $table->foreign('posts_id')->references('id')
                ->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_posts');
    }
}
