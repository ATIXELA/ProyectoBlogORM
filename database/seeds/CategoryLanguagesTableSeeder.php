<?php

use Illuminate\Database\Seeder;
use App\Models\CategoryLanguage;
use App\Models\Language;
use App\Models\Category;

class CategoryLanguagesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
        //$faker = Faker\Factory::create();
        factory(CategoryLanguage::class,8)->create();
  }
}
